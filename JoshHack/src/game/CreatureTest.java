package game;

import static org.junit.Assert.*;

import java.awt.Color;

import org.junit.Before;
import org.junit.Test;

public class CreatureTest {
	private Creature c;
	private int val;
	@Before
	public void setUp() throws Exception {
		Tile tiles[][][] = new Tile [1][1][1];
		World w = new World(tiles);
		val = 5;
		c = new Creature(w, 'g', new Color(5), "Gryphon", 0, 0, 0);
	}

	@Test
	public void testModHp() {
		c.modifyMaxHp(val);
		assertTrue(c.maxHp() == 5);
	}
	@Test
	public void testModAtk()
	{
		c.modifyAttackValue(val);
		assertTrue(c.attackValue() == 5);
	}
	@Test
	public void testModDef()
	{
		c.modifyDefenseValue(val);
		assertTrue(c.defenseValue() == 5);
	}
	@Test
	public void testModVis()
	{
		c.modifyVisionRadius(val);
		assertTrue(c.visionRadius() == 14);
	}

}
