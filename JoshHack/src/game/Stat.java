package game;

public class Stat {
	private String name;
	private int val;
	public Stat(String name, int val)
	{
		this.name = name;
		this.val = val;
	}
	public String getName()
	{
		return name;
	}
	public int getVal()
	{
		return val;
	}
	public void setVal(int amount)
	{
		val = amount;
	}
	//the modify value method is the main reason this method exists
	public void modVal(int amount)
	{
		val += amount;
	}

}
